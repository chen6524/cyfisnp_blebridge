/*
 * Copyright (c) 2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PROJECTZERO_H
#define PROJECTZERO_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */
#include <bcomdef.h>
#include <ti/drivers/UART.h>
/*********************************************************************
*  EXTERNAL VARIABLES
*/

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * CONSTANTS
 */
#define UARTBLE_CMDSIZE                    64
#define UART_CONTROL_UART_STATE_IDLE       0
#define UART_CONTROL_UART_STATE_READ_LEN   1
#define UART_CONTROL_UART_STATE_DATA       2

#define UART_CONTROL_COMMAND_SCAN          0x0D
#define UART_CONTROL_COMMAND_SCAN1         0x0A
#define UART_CONTROL_COMMAND_RESET         0xC0
#define UART_CONTROL_COMMAND_READ          0xC9
#define UART_CONTROL_COMMAND_WRITE_REQUEST 0xCB
#define UART_CONTROL_COMMAND_NOTIFY        0x02
#define UART_CONTROL_COMMAND_INDICATE      0x03

#define UARBLE_INDEX                       0
#define UARTBLE_BAUDRATE                   115200

typedef struct t_UART_CONTROL_DATA {
    uint8_t type;
    uint8_t len;
    uint8_t data[UARTBLE_CMDSIZE];
} UART_CONTROL_DATA;

extern UART_Handle uartRead;
extern UART_Handle uartWrite;
extern UART_CONTROL_DATA uart_command;

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * FUNCTIONS
 */

/*
 * Task creation function for the Simple BLE Peripheral.
 */
extern void BLEBridge_createTask(void);


/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* PROJECTZERO_H */
